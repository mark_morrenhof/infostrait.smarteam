Friend Class STObject

    ' Stores 1 SmObject + TreeNode info (can be used on multiple treenodes ..)

    Private _SmObject As ISmObject

    Friend SmAllRevisions As ISmObjects
    Friend STAllRevisionsDict As New Dictionary(Of Integer, STObject)
    Friend AllRevisions As String  ' All revs in a string

    Friend ClassID As Short
    Friend ObjectID As Integer

    Friend IconID As Integer = 0

    Friend LCState As Integer
    Friend StatusInVault As Boolean

    Friend PrimID As String
    Friend Description As String
  
    ' File Info
    Friend FileName As String
    Friend FilePaths As New FilePaths


    Friend Property SmObject() As ISmObject
        Get
            Return _SmObject
        End Get
        Set(ByVal value As ISmObject)
            _SmObject = value
            ClassID = _SmObject.ClassId
            ObjectID = _SmObject.ObjectId
        End Set
    End Property

    Friend Sub AddRevision(ByVal STRevObject As STObject)

        Try
            STAllRevisionsDict.Add(STAllRevisionsDict.Count, STRevObject)
        Catch ex As Exception
            Throw New Exception("[Error in STObject.AddRevision: " & ex.Message & "]")
        End Try

    End Sub

End Class
