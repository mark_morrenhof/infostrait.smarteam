<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.Tree1 = New System.Windows.Forms.TreeView()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkAWLQuery = New System.Windows.Forms.CheckBox()
        Me.chkTestShowSQL = New System.Windows.Forms.CheckBox()
        Me.btnRefreshTree = New System.Windows.Forms.Button()
        Me.lblState = New System.Windows.Forms.Label()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkCreateBatchFile = New System.Windows.Forms.CheckBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.chkOnlyLogActions = New System.Windows.Forms.CheckBox()
        Me.btnCopy = New System.Windows.Forms.Button()
        Me.chkCopyRevisions = New System.Windows.Forms.CheckBox()
        Me.txtTempFolder = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.radCopyToTestVault = New System.Windows.Forms.RadioButton()
        Me.radCopyToTempFolder = New System.Windows.Forms.RadioButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtSourceFolder = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.chkShowLinkType = New System.Windows.Forms.CheckBox()
        Me.btnAllLinks = New System.Windows.Forms.Button()
        Me.chkLinkSolidWorksSuppressed = New System.Windows.Forms.CheckBox()
        Me.chkLinkSolidWorksReference = New System.Windows.Forms.CheckBox()
        Me.chkLinkSolidWorksRapidDraft = New System.Windows.Forms.CheckBox()
        Me.chkLinkSolidWorksInContext = New System.Windows.Forms.CheckBox()
        Me.chkLinkSolidWorksDrawingOf = New System.Windows.Forms.CheckBox()
        Me.chkLinkSolidWorksDerivedPart = New System.Windows.Forms.CheckBox()
        Me.chkLinkedeDrawingOf = New System.Windows.Forms.CheckBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.chkShowFilePath = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.chkShowDescription = New System.Windows.Forms.CheckBox()
        Me.chkSHowPrimID = New System.Windows.Forms.CheckBox()
        Me.chkShowRevisions = New System.Windows.Forms.CheckBox()
        Me.chkShowFileExists = New System.Windows.Forms.CheckBox()
        Me.chkShowObjectIDs = New System.Windows.Forms.CheckBox()
        Me.btnCopyText = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.lblTotalFilesCopied = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lblTotalDestAlreadyExists = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblTotalsFilesNotFound = New System.Windows.Forms.Label()
        Me.lblTotalsFilesFound = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblTotalSelected = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Tree1
        '
        Me.Tree1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Tree1.ImageIndex = 0
        Me.Tree1.ImageList = Me.ImageList1
        Me.Tree1.Location = New System.Drawing.Point(12, 12)
        Me.Tree1.Name = "Tree1"
        Me.Tree1.SelectedImageIndex = 0
        Me.Tree1.Size = New System.Drawing.Size(404, 403)
        Me.Tree1.TabIndex = 0
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "FOLDOPEN.BMP")
        Me.ImageList1.Images.SetKeyName(1, "Document.bmp")
        Me.ImageList1.Images.SetKeyName(2, "SolidWorks Part.bmp")
        Me.ImageList1.Images.SetKeyName(3, "SolidWorks Assembly.bmp")
        Me.ImageList1.Images.SetKeyName(4, "SolidWorks Drawing.bmp")
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.chkAWLQuery)
        Me.GroupBox1.Controls.Add(Me.chkTestShowSQL)
        Me.GroupBox1.Controls.Add(Me.btnRefreshTree)
        Me.GroupBox1.Controls.Add(Me.lblState)
        Me.GroupBox1.Controls.Add(Me.btnClose)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 450)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(868, 62)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'chkAdvancedQuery
        '
        Me.chkAWLQuery.AutoSize = True
        Me.chkAWLQuery.Location = New System.Drawing.Point(382, 39)
        Me.chkAWLQuery.Name = "chkAdvancedQuery"
        Me.chkAWLQuery.Size = New System.Drawing.Size(105, 17)
        Me.chkAWLQuery.TabIndex = 29
        Me.chkAWLQuery.Text = "AWL Test Query"
        Me.chkAWLQuery.UseVisualStyleBackColor = True
        '
        'chkTestShowSQL
        '
        Me.chkTestShowSQL.AutoSize = True
        Me.chkTestShowSQL.Location = New System.Drawing.Point(382, 16)
        Me.chkTestShowSQL.Name = "chkTestShowSQL"
        Me.chkTestShowSQL.Size = New System.Drawing.Size(118, 17)
        Me.chkTestShowSQL.TabIndex = 28
        Me.chkTestShowSQL.Text = "Show sql statement"
        Me.chkTestShowSQL.UseVisualStyleBackColor = True
        '
        'btnRefreshTree
        '
        Me.btnRefreshTree.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRefreshTree.Location = New System.Drawing.Point(628, 16)
        Me.btnRefreshTree.Name = "btnRefreshTree"
        Me.btnRefreshTree.Size = New System.Drawing.Size(114, 34)
        Me.btnRefreshTree.TabIndex = 27
        Me.btnRefreshTree.Text = "Refresh tree "
        Me.btnRefreshTree.UseVisualStyleBackColor = True
        '
        'lblState
        '
        Me.lblState.AutoSize = True
        Me.lblState.ForeColor = System.Drawing.Color.Maroon
        Me.lblState.Location = New System.Drawing.Point(17, 27)
        Me.lblState.Name = "lblState"
        Me.lblState.Size = New System.Drawing.Size(10, 13)
        Me.lblState.TabIndex = 2
        Me.lblState.Text = "-"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Location = New System.Drawing.Point(748, 16)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(114, 34)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.chkCreateBatchFile)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.chkOnlyLogActions)
        Me.GroupBox2.Controls.Add(Me.btnCopy)
        Me.GroupBox2.Controls.Add(Me.chkCopyRevisions)
        Me.GroupBox2.Controls.Add(Me.txtTempFolder)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.radCopyToTestVault)
        Me.GroupBox2.Controls.Add(Me.radCopyToTempFolder)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.txtSourceFolder)
        Me.GroupBox2.Location = New System.Drawing.Point(422, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(458, 204)
        Me.GroupBox2.TabIndex = 19
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Copy options"
        '
        'chkCreateBatchFile
        '
        Me.chkCreateBatchFile.AutoSize = True
        Me.chkCreateBatchFile.Location = New System.Drawing.Point(18, 181)
        Me.chkCreateBatchFile.Name = "chkCreateBatchFile"
        Me.chkCreateBatchFile.Size = New System.Drawing.Size(341, 17)
        Me.chkCreateBatchFile.TabIndex = 30
        Me.chkCreateBatchFile.Text = "Only create batch file, no check on file existance or copy operation"
        Me.chkCreateBatchFile.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(10, 112)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(71, 13)
        Me.Label9.TabIndex = 29
        Me.Label9.Text = "Copy options:"
        '
        'chkOnlyLogActions
        '
        Me.chkOnlyLogActions.AutoSize = True
        Me.chkOnlyLogActions.Checked = True
        Me.chkOnlyLogActions.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkOnlyLogActions.Location = New System.Drawing.Point(18, 161)
        Me.chkOnlyLogActions.Name = "chkOnlyLogActions"
        Me.chkOnlyLogActions.Size = New System.Drawing.Size(305, 17)
        Me.chkOnlyLogActions.TabIndex = 28
        Me.chkOnlyLogActions.Text = "Only log for test, check file existance but no copy operation"
        Me.chkOnlyLogActions.UseVisualStyleBackColor = True
        '
        'btnCopy
        '
        Me.btnCopy.Enabled = False
        Me.btnCopy.Location = New System.Drawing.Point(329, 128)
        Me.btnCopy.Name = "btnCopy"
        Me.btnCopy.Size = New System.Drawing.Size(114, 34)
        Me.btnCopy.TabIndex = 27
        Me.btnCopy.Text = "Copy"
        Me.btnCopy.UseVisualStyleBackColor = True
        '
        'chkCopyRevisions
        '
        Me.chkCopyRevisions.AutoSize = True
        Me.chkCopyRevisions.Checked = True
        Me.chkCopyRevisions.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkCopyRevisions.Location = New System.Drawing.Point(18, 138)
        Me.chkCopyRevisions.Name = "chkCopyRevisions"
        Me.chkCopyRevisions.Size = New System.Drawing.Size(107, 17)
        Me.chkCopyRevisions.TabIndex = 26
        Me.chkCopyRevisions.Text = "Copy all revisions"
        Me.chkCopyRevisions.UseVisualStyleBackColor = True
        '
        'txtTempFolder
        '
        Me.txtTempFolder.Location = New System.Drawing.Point(190, 95)
        Me.txtTempFolder.Name = "txtTempFolder"
        Me.txtTempFolder.Size = New System.Drawing.Size(255, 20)
        Me.txtTempFolder.TabIndex = 25
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(10, 49)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(88, 13)
        Me.Label4.TabIndex = 23
        Me.Label4.Text = "Copy destination:"
        '
        'radCopyToTestVault
        '
        Me.radCopyToTestVault.AutoSize = True
        Me.radCopyToTestVault.Location = New System.Drawing.Point(153, 49)
        Me.radCopyToTestVault.Name = "radCopyToTestVault"
        Me.radCopyToTestVault.Size = New System.Drawing.Size(206, 17)
        Me.radCopyToTestVault.TabIndex = 22
        Me.radCopyToTestVault.Text = "copy files to test Vault DB (current DB)"
        Me.radCopyToTestVault.UseVisualStyleBackColor = True
        '
        'radCopyToTempFolder
        '
        Me.radCopyToTempFolder.AutoSize = True
        Me.radCopyToTempFolder.Checked = True
        Me.radCopyToTempFolder.Location = New System.Drawing.Point(153, 72)
        Me.radCopyToTempFolder.Name = "radCopyToTempFolder"
        Me.radCopyToTempFolder.Size = New System.Drawing.Size(139, 17)
        Me.radCopyToTempFolder.TabIndex = 21
        Me.radCopyToTempFolder.TabStop = True
        Me.radCopyToTempFolder.Text = "copy files to temp folder:"
        Me.radCopyToTempFolder.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(137, 13)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Source location (vault root):"
        '
        'txtSourceFolder
        '
        Me.txtSourceFolder.Location = New System.Drawing.Point(153, 19)
        Me.txtSourceFolder.Name = "txtSourceFolder"
        Me.txtSourceFolder.Size = New System.Drawing.Size(255, 20)
        Me.txtSourceFolder.TabIndex = 19
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Controls.Add(Me.chkShowLinkType)
        Me.GroupBox3.Controls.Add(Me.btnAllLinks)
        Me.GroupBox3.Controls.Add(Me.chkLinkSolidWorksSuppressed)
        Me.GroupBox3.Controls.Add(Me.chkLinkSolidWorksReference)
        Me.GroupBox3.Controls.Add(Me.chkLinkSolidWorksRapidDraft)
        Me.GroupBox3.Controls.Add(Me.chkLinkSolidWorksInContext)
        Me.GroupBox3.Controls.Add(Me.chkLinkSolidWorksDrawingOf)
        Me.GroupBox3.Controls.Add(Me.chkLinkSolidWorksDerivedPart)
        Me.GroupBox3.Controls.Add(Me.chkLinkedeDrawingOf)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Controls.Add(Me.chkShowFilePath)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.chkShowDescription)
        Me.GroupBox3.Controls.Add(Me.chkSHowPrimID)
        Me.GroupBox3.Controls.Add(Me.chkShowRevisions)
        Me.GroupBox3.Controls.Add(Me.chkShowFileExists)
        Me.GroupBox3.Controls.Add(Me.chkShowObjectIDs)
        Me.GroupBox3.Location = New System.Drawing.Point(426, 222)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(255, 222)
        Me.GroupBox3.TabIndex = 20
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Tree options"
        '
        'chkShowLinkType
        '
        Me.chkShowLinkType.AutoSize = True
        Me.chkShowLinkType.Location = New System.Drawing.Point(14, 114)
        Me.chkShowLinkType.Name = "chkShowLinkType"
        Me.chkShowLinkType.Size = New System.Drawing.Size(69, 17)
        Me.chkShowLinkType.TabIndex = 37
        Me.chkShowLinkType.Text = "Link type"
        Me.chkShowLinkType.UseVisualStyleBackColor = True
        '
        'btnAllLinks
        '
        Me.btnAllLinks.Location = New System.Drawing.Point(217, 19)
        Me.btnAllLinks.Name = "btnAllLinks"
        Me.btnAllLinks.Size = New System.Drawing.Size(32, 23)
        Me.btnAllLinks.TabIndex = 36
        Me.btnAllLinks.Text = "all"
        Me.btnAllLinks.UseVisualStyleBackColor = True
        '
        'chkLinkSolidWorksSuppressed
        '
        Me.chkLinkSolidWorksSuppressed.AutoSize = True
        Me.chkLinkSolidWorksSuppressed.Location = New System.Drawing.Point(128, 183)
        Me.chkLinkSolidWorksSuppressed.Name = "chkLinkSolidWorksSuppressed"
        Me.chkLinkSolidWorksSuppressed.Size = New System.Drawing.Size(103, 17)
        Me.chkLinkSolidWorksSuppressed.TabIndex = 35
        Me.chkLinkSolidWorksSuppressed.Text = "SW Suppressed"
        Me.chkLinkSolidWorksSuppressed.UseVisualStyleBackColor = True
        '
        'chkLinkSolidWorksReference
        '
        Me.chkLinkSolidWorksReference.AutoSize = True
        Me.chkLinkSolidWorksReference.Location = New System.Drawing.Point(128, 160)
        Me.chkLinkSolidWorksReference.Name = "chkLinkSolidWorksReference"
        Me.chkLinkSolidWorksReference.Size = New System.Drawing.Size(97, 17)
        Me.chkLinkSolidWorksReference.TabIndex = 34
        Me.chkLinkSolidWorksReference.Text = "SW Reference"
        Me.chkLinkSolidWorksReference.UseVisualStyleBackColor = True
        '
        'chkLinkSolidWorksRapidDraft
        '
        Me.chkLinkSolidWorksRapidDraft.AutoSize = True
        Me.chkLinkSolidWorksRapidDraft.Location = New System.Drawing.Point(127, 137)
        Me.chkLinkSolidWorksRapidDraft.Name = "chkLinkSolidWorksRapidDraft"
        Me.chkLinkSolidWorksRapidDraft.Size = New System.Drawing.Size(98, 17)
        Me.chkLinkSolidWorksRapidDraft.TabIndex = 33
        Me.chkLinkSolidWorksRapidDraft.Text = "SW RapidDraft"
        Me.chkLinkSolidWorksRapidDraft.UseVisualStyleBackColor = True
        '
        'chkLinkSolidWorksInContext
        '
        Me.chkLinkSolidWorksInContext.AutoSize = True
        Me.chkLinkSolidWorksInContext.Location = New System.Drawing.Point(127, 114)
        Me.chkLinkSolidWorksInContext.Name = "chkLinkSolidWorksInContext"
        Me.chkLinkSolidWorksInContext.Size = New System.Drawing.Size(95, 17)
        Me.chkLinkSolidWorksInContext.TabIndex = 32
        Me.chkLinkSolidWorksInContext.Text = "SW In Context"
        Me.chkLinkSolidWorksInContext.UseVisualStyleBackColor = True
        '
        'chkLinkSolidWorksDrawingOf
        '
        Me.chkLinkSolidWorksDrawingOf.AutoSize = True
        Me.chkLinkSolidWorksDrawingOf.Checked = True
        Me.chkLinkSolidWorksDrawingOf.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkLinkSolidWorksDrawingOf.Location = New System.Drawing.Point(127, 91)
        Me.chkLinkSolidWorksDrawingOf.Name = "chkLinkSolidWorksDrawingOf"
        Me.chkLinkSolidWorksDrawingOf.Size = New System.Drawing.Size(98, 17)
        Me.chkLinkSolidWorksDrawingOf.TabIndex = 31
        Me.chkLinkSolidWorksDrawingOf.Text = "SW Drawing of"
        Me.chkLinkSolidWorksDrawingOf.UseVisualStyleBackColor = True
        '
        'chkLinkSolidWorksDerivedPart
        '
        Me.chkLinkSolidWorksDerivedPart.AutoSize = True
        Me.chkLinkSolidWorksDerivedPart.Location = New System.Drawing.Point(127, 68)
        Me.chkLinkSolidWorksDerivedPart.Name = "chkLinkSolidWorksDerivedPart"
        Me.chkLinkSolidWorksDerivedPart.Size = New System.Drawing.Size(106, 17)
        Me.chkLinkSolidWorksDerivedPart.TabIndex = 30
        Me.chkLinkSolidWorksDerivedPart.Text = "SW Derived Part"
        Me.chkLinkSolidWorksDerivedPart.UseVisualStyleBackColor = True
        '
        'chkLinkedeDrawingOf
        '
        Me.chkLinkedeDrawingOf.AutoSize = True
        Me.chkLinkedeDrawingOf.Location = New System.Drawing.Point(127, 45)
        Me.chkLinkedeDrawingOf.Name = "chkLinkedeDrawingOf"
        Me.chkLinkedeDrawingOf.Size = New System.Drawing.Size(83, 17)
        Me.chkLinkedeDrawingOf.TabIndex = 29
        Me.chkLinkedeDrawingOf.Text = "eDrawing of"
        Me.chkLinkedeDrawingOf.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(101, 20)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(109, 13)
        Me.Label8.TabIndex = 28
        Me.Label8.Text = "Retrieve links of type:"
        '
        'chkShowFilePath
        '
        Me.chkShowFilePath.AutoSize = True
        Me.chkShowFilePath.Location = New System.Drawing.Point(14, 160)
        Me.chkShowFilePath.Name = "chkShowFilePath"
        Me.chkShowFilePath.Size = New System.Drawing.Size(66, 17)
        Me.chkShowFilePath.TabIndex = 27
        Me.chkShowFilePath.Text = "File path"
        Me.chkShowFilePath.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(5, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 13)
        Me.Label2.TabIndex = 25
        Me.Label2.Text = "Show in tree:"
        '
        'chkShowDescription
        '
        Me.chkShowDescription.AutoSize = True
        Me.chkShowDescription.Location = New System.Drawing.Point(14, 68)
        Me.chkShowDescription.Name = "chkShowDescription"
        Me.chkShowDescription.Size = New System.Drawing.Size(79, 17)
        Me.chkShowDescription.TabIndex = 5
        Me.chkShowDescription.Text = "Description"
        Me.chkShowDescription.UseVisualStyleBackColor = True
        '
        'chkSHowPrimID
        '
        Me.chkSHowPrimID.AutoSize = True
        Me.chkSHowPrimID.Checked = True
        Me.chkSHowPrimID.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkSHowPrimID.Location = New System.Drawing.Point(14, 45)
        Me.chkSHowPrimID.Name = "chkSHowPrimID"
        Me.chkSHowPrimID.Size = New System.Drawing.Size(79, 17)
        Me.chkSHowPrimID.TabIndex = 4
        Me.chkSHowPrimID.Text = "Primary IDs"
        Me.chkSHowPrimID.UseVisualStyleBackColor = True
        '
        'chkShowRevisions
        '
        Me.chkShowRevisions.AutoSize = True
        Me.chkShowRevisions.Location = New System.Drawing.Point(14, 137)
        Me.chkShowRevisions.Name = "chkShowRevisions"
        Me.chkShowRevisions.Size = New System.Drawing.Size(86, 17)
        Me.chkShowRevisions.TabIndex = 3
        Me.chkShowRevisions.Text = "All Revisions"
        Me.chkShowRevisions.UseVisualStyleBackColor = True
        '
        'chkShowFileExists
        '
        Me.chkShowFileExists.AutoSize = True
        Me.chkShowFileExists.ForeColor = System.Drawing.Color.Navy
        Me.chkShowFileExists.Location = New System.Drawing.Point(14, 183)
        Me.chkShowFileExists.Name = "chkShowFileExists"
        Me.chkShowFileExists.Size = New System.Drawing.Size(72, 17)
        Me.chkShowFileExists.TabIndex = 1
        Me.chkShowFileExists.Text = "File Exists"
        Me.chkShowFileExists.UseVisualStyleBackColor = True
        '
        'chkShowObjectIDs
        '
        Me.chkShowObjectIDs.AutoSize = True
        Me.chkShowObjectIDs.Location = New System.Drawing.Point(14, 91)
        Me.chkShowObjectIDs.Name = "chkShowObjectIDs"
        Me.chkShowObjectIDs.Size = New System.Drawing.Size(107, 17)
        Me.chkShowObjectIDs.TabIndex = 0
        Me.chkShowObjectIDs.Text = "ClassID ObjectID"
        Me.chkShowObjectIDs.UseVisualStyleBackColor = True
        '
        'btnCopyText
        '
        Me.btnCopyText.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCopyText.Location = New System.Drawing.Point(345, 421)
        Me.btnCopyText.Name = "btnCopyText"
        Me.btnCopyText.Size = New System.Drawing.Size(75, 23)
        Me.btnCopyText.TabIndex = 21
        Me.btnCopyText.Text = "copy text"
        Me.btnCopyText.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox4.Controls.Add(Me.lblTotalFilesCopied)
        Me.GroupBox4.Controls.Add(Me.Label10)
        Me.GroupBox4.Controls.Add(Me.lblTotalDestAlreadyExists)
        Me.GroupBox4.Controls.Add(Me.Label5)
        Me.GroupBox4.Controls.Add(Me.lblTotalsFilesNotFound)
        Me.GroupBox4.Controls.Add(Me.lblTotalsFilesFound)
        Me.GroupBox4.Controls.Add(Me.Label7)
        Me.GroupBox4.Controls.Add(Me.Label6)
        Me.GroupBox4.Controls.Add(Me.lblTotalSelected)
        Me.GroupBox4.Controls.Add(Me.Label3)
        Me.GroupBox4.Location = New System.Drawing.Point(692, 225)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(188, 219)
        Me.GroupBox4.TabIndex = 22
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Totals"
        '
        'lblTotalFilesCopied
        '
        Me.lblTotalFilesCopied.AutoSize = True
        Me.lblTotalFilesCopied.Location = New System.Drawing.Point(139, 134)
        Me.lblTotalFilesCopied.Name = "lblTotalFilesCopied"
        Me.lblTotalFilesCopied.Size = New System.Drawing.Size(10, 13)
        Me.lblTotalFilesCopied.TabIndex = 9
        Me.lblTotalFilesCopied.Text = "-"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(9, 135)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(132, 13)
        Me.Label10.TabIndex = 8
        Me.Label10.Text = "Files copied or in batchfile:"
        '
        'lblTotalDestAlreadyExists
        '
        Me.lblTotalDestAlreadyExists.AutoSize = True
        Me.lblTotalDestAlreadyExists.Location = New System.Drawing.Point(139, 112)
        Me.lblTotalDestAlreadyExists.Name = "lblTotalDestAlreadyExists"
        Me.lblTotalDestAlreadyExists.Size = New System.Drawing.Size(10, 13)
        Me.lblTotalDestAlreadyExists.TabIndex = 7
        Me.lblTotalDestAlreadyExists.Text = "-"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 111)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(125, 13)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Dest. Files already exists:"
        '
        'lblTotalsFilesNotFound
        '
        Me.lblTotalsFilesNotFound.AutoSize = True
        Me.lblTotalsFilesNotFound.Location = New System.Drawing.Point(139, 83)
        Me.lblTotalsFilesNotFound.Name = "lblTotalsFilesNotFound"
        Me.lblTotalsFilesNotFound.Size = New System.Drawing.Size(10, 13)
        Me.lblTotalsFilesNotFound.TabIndex = 5
        Me.lblTotalsFilesNotFound.Text = "-"
        '
        'lblTotalsFilesFound
        '
        Me.lblTotalsFilesFound.AutoSize = True
        Me.lblTotalsFilesFound.Location = New System.Drawing.Point(139, 60)
        Me.lblTotalsFilesFound.Name = "lblTotalsFilesFound"
        Me.lblTotalsFilesFound.Size = New System.Drawing.Size(10, 13)
        Me.lblTotalsFilesFound.TabIndex = 4
        Me.lblTotalsFilesFound.Text = "-"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(9, 83)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(116, 13)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "Source Files not found:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(9, 60)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(98, 13)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Source Files found:"
        '
        'lblTotalSelected
        '
        Me.lblTotalSelected.AutoSize = True
        Me.lblTotalSelected.Location = New System.Drawing.Point(139, 26)
        Me.lblTotalSelected.Name = "lblTotalSelected"
        Me.lblTotalSelected.Size = New System.Drawing.Size(10, 13)
        Me.lblTotalSelected.TabIndex = 1
        Me.lblTotalSelected.Text = "-"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 26)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Selected in ST:"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(892, 524)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.btnCopyText)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Tree1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SmarTeam Copy Files"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Tree1 As System.Windows.Forms.TreeView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents lblState As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtTempFolder As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents radCopyToTestVault As System.Windows.Forms.RadioButton
    Friend WithEvents radCopyToTempFolder As System.Windows.Forms.RadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSourceFolder As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents chkShowRevisions As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowFileExists As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowObjectIDs As System.Windows.Forms.CheckBox
    Friend WithEvents btnCopy As System.Windows.Forms.Button
    Friend WithEvents chkCopyRevisions As System.Windows.Forms.CheckBox
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents chkShowDescription As System.Windows.Forms.CheckBox
    Friend WithEvents chkSHowPrimID As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnCopyText As System.Windows.Forms.Button
    Friend WithEvents chkShowFilePath As System.Windows.Forms.CheckBox
    Friend WithEvents btnRefreshTree As System.Windows.Forms.Button
    Friend WithEvents chkOnlyLogActions As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents lblTotalSelected As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblTotalsFilesNotFound As System.Windows.Forms.Label
    Friend WithEvents lblTotalsFilesFound As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents chkLinkSolidWorksReference As System.Windows.Forms.CheckBox
    Friend WithEvents chkLinkSolidWorksRapidDraft As System.Windows.Forms.CheckBox
    Friend WithEvents chkLinkSolidWorksInContext As System.Windows.Forms.CheckBox
    Friend WithEvents chkLinkSolidWorksDrawingOf As System.Windows.Forms.CheckBox
    Friend WithEvents chkLinkSolidWorksDerivedPart As System.Windows.Forms.CheckBox
    Friend WithEvents chkLinkedeDrawingOf As System.Windows.Forms.CheckBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents chkLinkSolidWorksSuppressed As System.Windows.Forms.CheckBox
    Friend WithEvents chkCreateBatchFile As System.Windows.Forms.CheckBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents btnAllLinks As System.Windows.Forms.Button
    Friend WithEvents chkShowLinkType As System.Windows.Forms.CheckBox
    Friend WithEvents chkTestShowSQL As System.Windows.Forms.CheckBox
    Friend WithEvents lblTotalDestAlreadyExists As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblTotalFilesCopied As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents chkAWLQuery As System.Windows.Forms.CheckBox

End Class
