Imports SmarTeamCopyFiles.SharedObjects

Friend Class General

    Friend MainForm As frmMain

    ' Private _Copiedfiles As Integer
    Private _LogFile As String = Nothing
    Private _BatchFile As String = Nothing

    Private _DBModel As DBModel
    Friend _STObjects As ISmObjects         ' TO PRIVATE

    Private _LogCommaSeparated As Boolean = True
    Private _LogLine As String = ""
    Const STARTLOG As String = "QSTART"
    Const WRITELOGLINE As String = "QWRITELOGLINE"

    Friend DescriptionAttributeFromConfig As String = ""

    Private _OnlyWriteBatchFile As Boolean = False
    Private _BatchFileWriter As IO.StreamWriter = Nothing
    Friend XCopyParameters As String = ""


    Friend _STObjectsALL As ISmObjects

    Private _CounterAddNewSTObject As Integer
    Private _CounterReUseSTObject As Integer
    Private _CounterFilesFound, _CounterFilesNotFound As Integer

    Private _DictAllRetrievedSmObjects As New Dictionary(Of Integer, STObject)

    '' For Copy operations
    Private TotalFilesFailed, Totals_SourceFound, Totals_DestExist, Totals_SourceNotFound, TotalFilesCopied As Integer
    Private _DoCopyAllRevisions, _DoOnlyLogNoActionsOnFiles, _DoCopyToVaultElseTempFolder As Boolean
    Private _CopySourceFolder, _DestinationFolder As String


    Friend Function GetSWLinkClasses() As String

        ' Description: 
        ' Invoked by : RefreshTree

        Dim s As String

        Try
            ' Get Link Class Types
            s = _DBModel.STClass_Tree.ClassId.ToString

            If MainForm.chkLinkedeDrawingOf.Checked Then
                s &= "," & _DBModel.STClass_SW_eDrawingOf.ClassId
            End If
            If MainForm.chkLinkSolidWorksDerivedPart.Checked Then
                s &= "," & _DBModel.STClass_SW_DerivedPart.ClassId
            End If
            If MainForm.chkLinkSolidWorksDrawingOf.Checked Then
                s &= "," & _DBModel.STClass_SW_DrawingOf.ClassId
            End If
            If MainForm.chkLinkSolidWorksInContext.Checked Then
                s &= "," & _DBModel.STClass_SW_InContext.ClassId
            End If
            If MainForm.chkLinkSolidWorksRapidDraft.Checked Then
                s &= "," & _DBModel.STClass_SW_RapidDraft.ClassId
            End If
            If MainForm.chkLinkSolidWorksReference.Checked Then
                s &= "," & _DBModel.STClass_SW_Reference.ClassId
            End If
            If MainForm.chkLinkSolidWorksSuppressed.Checked Then
                s &= "," & _DBModel.STClass_SW_Suppressed.ClassId
            End If

            Return s

        Catch ex As Exception
            Throw New Exception("[Error in GetSWLinkClasses: " & ex.Message & "]")
        End Try
    End Function

    Friend Function GetSelected() As Integer

        'Me.txtSelected.Text = "0"

        Try

            ' TEST: Get all ASSM
            If False Then

                'Dim STQ As ISmSimpleQuery
                'STQ = STSession.ObjectStore.NewSimpleQuery
                'STQ.RunQueryMode = RunQueryModeEnum.rqmDirect
                'STQ.SelectStatement = "Select CLASS_ID, OBJECT_ID from TN_DOCUMENTATION where CLASS_ID=715 order by CN_ID"
                'STQ.Run()
                '_STObjectsALL = STSession.ObjectStore.ObjectsFromData(STQ.QueryResult, True)

                '_STObjects = STSession.ObjectStore.NewObjects
                '_STObjects.Add(_STObjectsALL.Item(0).Clone)

            Else


                Try
                    _STObjects = STGUI.ActiveViewWindow.SmView.Selected.Objects.Clone
                Catch
                End Try
            End If


            If _STObjects Is Nothing Then
                MainForm.lblTotalSelected.Text = "0"
                Return 0
            Else

                ' TODO: Check al objects same superclass

                MainForm.lblTotalSelected.Text = _STObjects.Count.ToString

                _DBModel = New DBModel

                _DBModel.STClass_Super = STSession.MetaInfo.SmClass(_STObjects.Item(0).SmClass.SuperClassId)
                _DBModel.STClass_Tree = STSession.MetaInfo.SmClass(_DBModel.STClass_Super.DefaultHierachicalClassId)

                ' 300513: Use Descriptin attribu from config
                _DBModel.AttribOrder = "OBJECT_ID"
                _DBModel.AttribDescription = DescriptionAttributeFromConfig   ' "CN_DESCRIPTION"

                Return _STObjects.Count

            End If

        Catch ex As Exception
            Throw New Exception("[Error in GetSelected: " & ex.Message & "]")
        End Try
    End Function

    Friend Function GetSmarTeamSession() As Boolean

        Dim myApplication As ISmApplication = Nothing


        ' Get running SmarTeam session
        Try
            myApplication = DirectCast(GetObject(, "smarteam.application"), ISmApplication)
        Catch
        End Try
        If myApplication Is Nothing Then
            Return False
        End If

        Try
            STSession = myApplication.Engine.Sessions(0)
            System.Runtime.InteropServices.Marshal.ReleaseComObject(myApplication)
        Catch
        End Try
        If STSession Is Nothing Then
            Return False
        End If

        Try
            STGUI = DirectCast(STSession.GetService("SmGUISrv.SmGUIServices"), ISmGUIServices)
        Catch
        End Try
        If STGUI Is Nothing Then
            Return False
        End If

        STQuery1 = STSession.ObjectStore.NewSimpleQuery
        STQuery1.RunQueryMode = RunQueryModeEnum.rqmDirect

        _DBModel = New DBModel



        Return True

    End Function


    Friend Function RetrieveSTObject(ByVal SmObject As ISmObject, ByVal DoRefresh As Boolean, ByVal DoRetrieveAllRevs As Boolean) As STObject

        ' Description: Retrieve all info for the specific SmObject, store data in a STObject and add to dictionary.
        '              In case DoRetrieveAllRevs do retrieve all other Revisions of this SmObject
        ' 
        ' Invoked by :

        Dim mySTObject As New STObject
        Dim mySTObjectRev As New STObject
        Dim SmRevObject As ISmObject


        Dim g As String

        Try
            ' In Dict?
            If Not _DictAllRetrievedSmObjects.ContainsKey(SmObject.ObjectId) Then

                Dim s As String

                '' Add to Dict
                mySTObject = New STObject
                mySTObject.SmObject = SmObject.Clone

                '' All Attributes
                If DoRefresh Then
                    mySTObject.SmObject.AddAllAttributes()
                    mySTObject.SmObject.Retrieve()
                End If

                ' Tree Icon
                mySTObject.IconID = GetIconID(mySTObject.SmObject.SmClass.Name)

                ' Status
                mySTObject.LCState = mySTObject.SmObject.Data.ValueAsInteger("STATE")
                Select Case mySTObject.LCState
                    Case STATE_CHECKED_IN, STATE_RELEASED, STATE_OBSOLETE
                        mySTObject.StatusInVault = True
                End Select

                ' File Info
                s = mySTObject.SmObject.Data.ValueAsString("FILE_NAME")
                If s Is Nothing Then s = ""
                mySTObject.FileName = s

                If mySTObject.StatusInVault And (mySTObject.FileName.Length > 0) Then

                    ' 070910: Changed this check
                    s = mySTObject.SmObject.Data.ValueAsString("DIRECTORY")
                    If s Is Nothing Then s = ""
                    If s.Trim.Length = 0 Then
                        '' Check in Vault by VaultObjectID                          
                        mySTObject.FilePaths = _DBModel.GetVaultPathByID(mySTObject.SmObject.Data.ValueAsInteger("VAULT_OBJECT_ID"))
                        ' s = myFilePaths.FilePathFromRoot
                    End If
                    '' File Path
                    'If s.Length > 0 Then
                    '    mySTObject.FilePath = System.IO.Path.Combine(s, mySTObject.FileName)
                    'End If
                Else
                    ' Note: myFPS.IsVaultPath remains False
                End If

                ' Get all its Revisions!!
                If DoRetrieveAllRevs And MainForm.chkShowRevisions.Checked Then

                    ' Retrieve all revisions
                    mySTObject.SmAllRevisions = mySTObject.SmObject.RetrieveAllRevisionsSortedByHistory(mySTObject.SmObject.SmClass.PrimaryIdentifier)
                    mySTObject.AllRevisions = ""
                    For i As Integer = 0 To mySTObject.SmAllRevisions.Count - 1

                        SmRevObject = mySTObject.SmAllRevisions.Item(i).Clone
                        SmRevObject.AddAllAttributes()
                        SmRevObject.Retrieve()

                        ' Not for current SmObjects revision
                        If SmRevObject.ObjectId <> SmObject.ObjectId Then
                            ' Create a new STObject for this SmObject and add to the current mySTObject
                            ' So call myself recursive!
                            mySTObjectRev = RetrieveSTObject(SmRevObject, False, False)
                            mySTObject.AddRevision(mySTObjectRev)
                        Else
                            ' ..
                        End If

                        ' Place in Concatenation String
                        If i > 0 Then
                            mySTObject.AllRevisions &= ","
                        End If
                        g = SmRevObject.Data.ValueAsString("REVISION")
                        If g Is Nothing Then g = ""
                        If g.Length = 0 Then
                            g = "_"
                        End If
                        mySTObject.AllRevisions &= g

                    Next i

                End If

                ' Add to Dict
                _CounterAddNewSTObject += 1
                _DictAllRetrievedSmObjects.Add(mySTObject.SmObject.ObjectId, mySTObject)

            Else
                ' Already in dict, re-use 
                _CounterReUseSTObject += 1
            End If


            Dim ReturnVal As STObject

            ReturnVal = _DictAllRetrievedSmObjects.Item(SmObject.ObjectId)
            If ReturnVal Is Nothing Then
                MsgBox("no tag STOBJECT!")
            End If

            Return ReturnVal

            ' \Return (_DictAllRetrievedSmObjects.Item(SmObject.ObjectId))

        Catch ex As Exception
            Throw New Exception("[Error in GetSTObject: " & ex.Message & "]")
        End Try

    End Function

    Friend Function LoadSmObjectsInTreeView(ByVal FolderName As String, ByVal TreeClasses As String) As Boolean

        ' Description: Get Settings folder and classification tree below
        '              Store in myTreeView and myNodesTable
        ' Invoked by : GetClassificationTree

        Dim TreeClassID As Short
        Dim sstep As String = "init"

        Dim FieldClass, FieldObject As String
        Dim ParentObjectId As Integer
        Dim ClsDescription As String = ""
        Dim DescrPrefix As String = ""
        Dim sql, s As String
        Dim i, Rows, iCount As Integer
        Dim parentNode, NewTreeNode, NewTreeNodeDwg As TreeNode
        'Dim ProgressForm As frmLoading = Nothing

        ' stacks
        Dim objectIDStack, treeNodesStack As Stack
        ' ST
        Dim STQuery As ISmSimpleQuery
        Dim mySTObject As STObject
        Dim _STQueryDefForDwgs As ISmQueryDefinition
        Dim STLinkedDrawings As ISmObjects

        Try
            MainForm.Tree1.Nodes.Clear()

            _CounterAddNewSTObject = 0
            _CounterReUseSTObject = 0

            _CounterFilesFound = 0
            _CounterFilesNotFound = 0

            _DictAllRetrievedSmObjects.Clear()


            System.Windows.Forms.Application.DoEvents()

            MainForm.Tree1.BeginUpdate()

            ' Define Hierarchical link query
            'sql = "select tree.CLASS_ID TREECLASSID, sett.CLASS_ID, sett.OBJECT_ID" & _
            '    " from " & _DBModel.STClass_Super.TableName & " sett," & _
            '               _DBModel.STClass_Tree.TableName & " tree" & _
            '    " where tree.PAR_OBJECT_ID=$ParObjectId" & _
            '    " and tree.SON_OBJECT_ID=sett.OBJECT_ID" & _
            '    " and tree.CLASS_ID in (" & TreeClasses & ")" & _
            '    " order by lower(sett." & _DBModel.AttribOrder & ")"

            ''; ' , tree.CLASS_ID TCID" & _

            ' Define Hierarchical link query > Without Son
            'sql = "select OBJECT_ID, CLASS_ID, CLASS_ID2, OBJECT_ID2 " & _
            '    " from " & _DBModel.STClass_Tree.TableName & " tree" & _
            '    " where tree.PAR_OBJECT_ID=$ParObjectId" & _
            '    " and TREE.CLASS_ID in (" & TreeClasses & ")"


            ' Define Hierarchical link query > Without Son
            If Not MainForm.chkAWLQuery.Checked Then

                If SQLSERVERDB Then
                    ' 210711: solved bug TREE>tree
                    sql = "select OBJECT_ID, CLASS_ID, CLASS_ID2, OBJECT_ID2" & _
                        " from " & _DBModel.STClass_Tree.TableName & " tree" & _
                        " where PAR_OBJECT_ID=$ParObjectId" & _
                        " and CLASS_ID in (" & TreeClasses & ")"
                Else
                    ' 210711: solved bug TREE>tree
                    sql = "select OBJECT_ID, CLASS_ID, CLASS_ID2, OBJECT_ID2" & _
                        " from " & _DBModel.STClass_Tree.TableName & " tree" & _
                        " where tree.PAR_OBJECT_ID=$ParObjectId" & _
                        " and tree.CLASS_ID in (" & TreeClasses & ")"
                End If


            Else
                ' AWL MechDes Query
                'sql = "select OBJECT_ID, CLASS_ID, CLASS_ID1, OBJECT_ID1, CLASS_ID2, OBJECT_ID2 " & _
                '      " from TDM_LINKS_00005 tree" & _
                '      " where (tree.OBJECT_ID1=$ParObjectId or tree.OBJECT_ID2=$ParObjectId)" & _
                '      " and tree.CLASS_ID in (" & TreeClasses & ")"

                sql = "select OBJECT_ID, CLASS_ID, CLASS_ID1, OBJECT_ID1, CLASS_ID2, OBJECT_ID2 " & _
                    " from TDM_LINKS_00005 tree" & _
                    " where tree.OBJECT_ID1=$ParObjectId" & _
                    " and tree.CLASS_ID in (" & TreeClasses & ")"
            End If

            If MainForm.chkTestShowSQL.Checked Then
                MessageBox.Show(sql, "Test message", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Clipboard.SetText(sql)
            End If

            'If MainForm.chkAdvancedQuery.Checked Then
            '    MessageBox.Show("Resultaat: teveel resultaten, tool loopt vast, dus nu stoppen ..")
            '    Return False
            'End If

            '' Drawings
            _STQueryDefForDwgs = STSession.ObjectStore.NewQueryDefinition
            _STQueryDefForDwgs.Roles.Add(_DBModel.STClass_SW_DrawingOf.ClassId, "L")
            _STQueryDefForDwgs.Roles.Add(_DBModel.STClass_Super.ClassId, "S")
            _STQueryDefForDwgs.LinkQueryDirection = LinkQueryDirectionEnum.lqdSecondToFirst
            _STQueryDefForDwgs.Select.Add("REVISION", "S", False)
            _STQueryDefForDwgs.Select.Add("CREATION_DATE", "S", False)


            '                
            '  MsgBox(sql)

            '   " sett." & STDBModel.AttribValue & ", sett." & STDBModel.AttribDescription & _

            STQuery = STSession.ObjectStore.NewSimpleQuery
            STQuery.SelectStatement = sql
            STQuery.Parameters.AddHeader("ParObjectId", 4, SmDataTypesEnum.sdtObjectIdentifier)

            '' Add top parent folder
            treeNodesStack = New Stack
            treeNodesStack.Push(MainForm.Tree1.Nodes.Add(FolderName))
            MainForm.Tree1.TopNode.ImageIndex = 0
            MainForm.Tree1.TopNode.SelectedImageIndex = 0

            ''
            '' Loop selected objects         
            ''
            For k As Integer = 0 To _STObjects.Count - 1

                '' Push parent Object in stack
                objectIDStack = New Stack
                objectIDStack.Push(_STObjects.Item(k).ObjectId)

                ' Retrieve child classifications recursively:
                treeNodesStack = New Stack

                mySTObject = RetrieveSTObject(_STObjects.Item(k).Clone, True, True)

                ' mySTObject = New STObject
                ' mySTObject.SmObject = _STObjects.Item(k)

                s = GetTreeNodeText(mySTObject, -1)
                NewTreeNode = MainForm.Tree1.TopNode.Nodes.Add(s)
                NewTreeNode.ImageIndex = mySTObject.IconID
                NewTreeNode.SelectedImageIndex = NewTreeNode.ImageIndex
                NewTreeNode.Tag = mySTObject

                treeNodesStack.Push(NewTreeNode)

                '090805: expand first folder node
                MainForm.Tree1.Nodes(0).Expand()

                Rows = 0
                iCount = 0
                While objectIDStack.Count > 0

                    iCount += 1
                    If iCount Mod (4) = 0 Then
                        System.Windows.Forms.Application.DoEvents()
                        MainForm.SetState("Loading trees #" & MainForm.Tree1.Nodes.Count)
                    End If


                    ' get parent node from top of stack
                    parentNode = DirectCast(treeNodesStack.Pop(), TreeNode)

              

                    ''
                    '' Query and loop sons, from last to first
                    ''
                    ParentObjectId = CInt(objectIDStack.Pop())


                    STQuery.Parameters.ValueAsInteger("ParObjectId", 0) = ParentObjectId
                    STQuery.Run()

                    ' STQuery.QueryResult.PrintToFile("33", "c:\TESTRL3.txt")

                    For i = 0 To STQuery.QueryResult.RecordCount - 1

                        FieldClass = "CLASS_ID2"
                        FieldObject = "OBJECT_ID2"

                        ' MsgBox("test")
                        sstep = "test"

                        If False Then ' MainForm.chkAWLQuery.Checked Then


                            If STQuery.QueryResult.ValueAsInteger(FieldObject, i) = ParentObjectId Then
                                ' 1-2 OK

                                ' MsgBox("12 OK")

                                sstep = "retrieve2"


                            Else

                                ' MsgBox("12 Switch")

                                sstep = "retrieve1"


                                ' Switch
                                FieldClass = "CLASS_ID1"
                                FieldObject = "OBJECT_ID1"
                            End If
                        Else
                            sstep = "retrieve"
                        End If


                        ' MsgBox("retrieve")
                        sstep = "retrieve"

                        mySTObject = RetrieveSTObject(STSession.ObjectStore.RetrieveObject(STQuery.QueryResult.ValueAsSmallInt(FieldClass, i), _
                                           STQuery.QueryResult.ValueAsInteger(FieldObject, i)), False, True)


                        ' MsgBox("ok")

                        sstep = "push"

                        ' push object id in stack so handled in main loop
                        objectIDStack.Push(mySTObject.SmObject.ObjectId)

                        sstep = "GetTree"


                        TreeClassID = STQuery.QueryResult.ValueAsSmallInt("CLASS_ID", i)

                        sstep = "GetNodeText"


                        ' create new treenode 
                        s = GetTreeNodeText(mySTObject, TreeClassID)
                        NewTreeNode = New TreeNode(s)
                        NewTreeNode.ImageIndex = mySTObject.IconID
                        NewTreeNode.SelectedImageIndex = NewTreeNode.ImageIndex
                        NewTreeNode.Tag = mySTObject

                        sstep = "Push2"


                        ' pust treenode in stack
                        treeNodesStack.Push(NewTreeNode)
                        ' add son node to parent node
                        parentNode.Nodes.Add(NewTreeNode)

                        Rows += 1

                        ' dssds()

                        '' Get Drawings
                        If MainForm.chkLinkSolidWorksDrawingOf.Checked Then

                            '' Query def for linke Drawings > Now IN LOOP BELOW

                            STLinkedDrawings = mySTObject.SmObject.RetrieveRelations(_STQueryDefForDwgs)
                            If STLinkedDrawings IsNot Nothing Then

                                '  MsgBox("Drawing-Of Link Class: " & _DBModel.STClass_SW_DrawingOf.ClassId & vbLf & "SuperClass: " & _DBModel.STClass_Super.ClassId & vbLf & vbLf & "> Drawings found:" & STLinkedDrawings.Count)

                                ' Loop Drawings
                                For d As Integer = 0 To STLinkedDrawings.Count - 1

                                    mySTObject = RetrieveSTObject(STLinkedDrawings.Item(d).Clone, True, True)

                                    ' Add as new treenode to the Tree
                                    s = GetTreeNodeText(mySTObject, CLASSIDDRAWINGS_FLAG)
                                    NewTreeNodeDwg = New TreeNode(s)
                                    NewTreeNodeDwg.ImageIndex = mySTObject.IconID
                                    NewTreeNodeDwg.SelectedImageIndex = NewTreeNodeDwg.ImageIndex
                                    NewTreeNodeDwg.Tag = mySTObject

                                    NewTreeNode.Nodes.Add(NewTreeNodeDwg)

                                Next d
                            End If

                        End If



                            'nodecount = MainForm.Tree1.Nodes.Count
                            'If nodecount Mod (10) = 0 Then
                            '    MainForm.SetState("Loading trees " & nodecount)
                            'End If

                            ' Update Totals in labels
                            ' UpdateTotalsOnForm()

                    Next i

                End While

            Next k

            MainForm.Tree1.EndUpdate()
            ' MainForm.Text &= " N:" & _CounterAddNewSTObject & " R:" & _CounterReUseSTObject

            ' Update Totals in labels
            ' UpdateTotalsOnForm()

            ' Finally Set order in table for Search functionality, and expand/collapse the nodes
            ' RecursiveLoopTree(TreeViewControl.Nodes(0), True)

            Return True

        Catch ex As Exception
            MessageBox.Show("Error occured in LoadClassificationInTreeView (step" & sstep & ": " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        Finally
        End Try
    End Function

    Private Function GetTreeNodeText(ByVal mySTObject As STObject, ByVal TreeClassID As Short) As String

        Dim s As String = ""
        Dim s2 As String
        Dim p As String
        Dim revs As String = ""

        Try
            ' PrimIDs
            If MainForm.chkSHowPrimID.Checked Then
                If mySTObject.PrimID Is Nothing Then
                    mySTObject.PrimID = mySTObject.SmObject.PrimaryAttributesAsString(" ")
                End If
                s &= mySTObject.PrimID
            End If

            ' Description
            If MainForm.chkShowDescription.Checked Then

                If mySTObject.Description Is Nothing Then
                    If _DBModel.AttribDescription IsNot Nothing Then
                        mySTObject.Description = mySTObject.SmObject.Data.ValueAsString(_DBModel.AttribDescription)
                    Else
                        mySTObject.Description = ""
                    End If
                End If
                s &= " " & mySTObject.Description
            End If

            ' All Revisions
            If MainForm.chkShowRevisions.Checked Then
                s &= " (revs:" & mySTObject.AllRevisions & ")"
            End If


            ' ObjectIDs
            If MainForm.chkShowObjectIDs.Checked Then
                s &= " [ID:" & mySTObject.ClassID & " " & mySTObject.ObjectID & "]"
            End If


            ' 30-11-2010: Treenode
            s2 = "?"
            If MainForm.chkShowLinkType.Checked Then
                If TreeClassID > 0 Then
                    Select Case TreeClassID
                        Case _DBModel.STClass_SW_eDrawingOf.ClassId : s2 = "SW eDrawing of"
                        Case _DBModel.STClass_SW_DerivedPart.ClassId : s2 = "SW Derived Part"
                        Case _DBModel.STClass_SW_DrawingOf.ClassId : s2 = "SW Drawing of"
                        Case _DBModel.STClass_SW_InContext.ClassId : s2 = "SW In Context"
                        Case _DBModel.STClass_SW_RapidDraft.ClassId : s2 = "SW RapidDraft"
                        Case _DBModel.STClass_SW_Reference.ClassId : s2 = "SW Reference"
                        Case _DBModel.STClass_SW_Suppressed.ClassId : s2 = "SW Suppressed"
                        Case _DBModel.STClass_Tree.ClassId : s2 = "Hierarchical"
                        Case CLASSIDDRAWINGS_FLAG : s2 = "Drawing"
                        Case Else
                            s2 = "Other:" & TreeClassID
                    End Select
                End If
                s &= " [LT:" & s2 & "]"
            End If

            ' File Exists
            If MainForm.chkShowFilePath.Checked Then
                If mySTObject.FilePaths.IsVaultPath Then
                    s &= " [PATH:" & mySTObject.FilePaths.VaultPath & mySTObject.FileName & "]"
                End If
            End If

            ' File Exists
            If MainForm.chkShowFileExists.Checked Then

                If mySTObject.StatusInVault Then
                    p = ""
                    If mySTObject.FilePaths.IsVaultPath Then
                        Try
                            If System.IO.File.Exists(mySTObject.FilePaths.VaultPath & mySTObject.FileName) Then
                                p = "FILE FOUND"
                                _CounterFilesFound += 1
                            Else
                                p = "FILE NOT FOUND"
                                _CounterFilesNotFound += 1
                            End If
                        Catch ex As Exception
                            p = "ERROR: " & ex.Message
                            _CounterFilesNotFound += 1
                        End Try


                    Else
                        p = "NO FILE INFO"
                    End If

                Else
                    p = "NEW OR CHECKEDOUT"
                End If
                s &= " [" & p & "]"
            End If

            Return s

        Catch ex As Exception
            Return s & " [ERROR GET INFO: " & ex.Message & "]"
        End Try

    End Function

 
    Private Function GetIconID(ByVal ClassName As String) As Integer

        If ClassName.IndexOf("Folder") > 0 Then
            Return 0
        ElseIf ClassName.IndexOf("Part") > 0 Then
            Return 2
        ElseIf ClassName.IndexOf("Assembly") > 0 Then
            Return 3
        ElseIf ClassName.IndexOf("Drawing") > 0 Then
            Return 4
        Else
            Return 1
        End If

    End Function


    Friend Function CopyAllFilesFromVault(ByVal DoOnlyLogNoActionsOnFiles As Boolean, _
                                            ByVal CopyAllRevisions As Boolean, _
                                            ByVal SourceFolder As String, _
                                            ByVal DestinationFolder As String, _
                                            ByVal DoCopyToVaultElseTempFolder As Boolean) As Integer

        ' Description: Only invoke RecursiveLoopTree for each subnode under the mainnode
        ' Invoked by : btnCopy_Click

        Try
            '' Reset totals etc

            _DoCopyAllRevisions = CopyAllRevisions
            _DoOnlyLogNoActionsOnFiles = DoOnlyLogNoActionsOnFiles
            _DoCopyToVaultElseTempFolder = DoCopyToVaultElseTempFolder
            _DestinationFolder = DestinationFolder
            _CopySourceFolder = SourceFolder

            If MainForm.chkCreateBatchFile.Checked Then

                ' Do not check file existing, just generate batch file
                _OnlyWriteBatchFile = True

                _BatchFile = IO.Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "CopyFiles.bat.txt")
                _BatchFileWriter = New IO.StreamWriter(_BatchFile, False)

            End If


            'MsgBox(MainForm.Tree1.TopNode.Text)

            ' All nodes under the parent FOLDER node
            For Each node As TreeNode In MainForm.Tree1.Nodes(0).Nodes '  .t.TopNode.Nodes
                RecursiveLoopTree(node)
            Next node

            Return TotalFilesCopied

        Catch ex As Exception
            MessageBox.Show("Error occured in CopyAllFilesFromVault: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return TotalFilesCopied
        Finally
            If _BatchFileWriter IsNot Nothing Then
                _BatchFileWriter.Close()
            End If
        End Try
    End Function

    Private Sub RecursiveLoopTree(ByVal TreeNode As TreeNode)

        ' Description: Call CopyFileFromVault for each TreeNode
        ' Invoked by : CopyAllFilesFromVault + RecursiveLoopTree (so resursice)

        Dim mySTObject As STObject

        Dim OtherRevs As Integer
        Dim s As String = ""
        Dim CopyMsg1 As String = ""
        Dim CopyMsg2 As String = ""
        Dim i As Integer

        Dim myStep As String = "init"

        Try
            If TreeNode.Tag Is Nothing Then
                TreeNode.Text &= "   [NO TREENODETAG ERROR]"
                Return
            Else
                mySTObject = DirectCast(TreeNode.Tag, STObject)
            End If

            myStep = ""
            MainForm.SetState("Copying" & mySTObject.PrimID & " ..")

            '' Copy
            If CopyFileFromVault(mySTObject, CopyMsg1) Then

                ' Copy all revisions? (only if Copy original succeeded!)
                If _DoCopyAllRevisions Then

                    MainForm.SetState("Copying" & mySTObject.PrimID & " ....")

                    For Each mySTOtherRev As STObject In mySTObject.STAllRevisionsDict.Values


                        ' Log
                        If i = 0 Then WriteLog("Other Revisions:")
                        i += 1

                        '' Copy other rev
                        If CopyFileFromVault(mySTOtherRev, CopyMsg2) Then
                            OtherRevs += 1
                        End If

                    Next mySTOtherRev
                End If

                s = " [RESULT: " & CopyMsg1
                If OtherRevs > 0 Then
                    s &= " + " & OtherRevs & " other Revs"
                End If
                s &= "]"

            Else
                ' File copy failed
                s = " [RESULT: " & CopyMsg1 & "]"
            End If

            TreeNode.Text &= s


            ' Loop Recursive
            For Each node As TreeNode In TreeNode.Nodes
                RecursiveLoopTree(node)
            Next node

            Return
        Catch ex As Exception
            Throw New Exception("[Error in RecursiveLoopTree: " & ex.Message & "]")
        End Try
    End Sub


    Private Function CopyFileFromVault(ByVal mySTObject As STObject, ByRef ReturnMsg As String) As Boolean

        ' Description: Copy a File from Vault and log to LogFile
        ' Invoked by : RecursiveLoopTree

        Dim mySourceFile, myDestinationFile As IO.FileInfo
        Dim myDestinationDirectory As IO.DirectoryInfo

        Dim s As String
        Dim DoGoOn, FileIsCopied As Boolean

        Try
            ' Object with file in Vault?
            If Not mySTObject.FilePaths.IsVaultPath Then
                ReturnMsg = "not correct vault paths"
                Return False
            End If

            ' Check already copied

            'ReturnMsg = "DISABLED"
            'Return True

            '' FileInfo Source
            mySourceFile = New IO.FileInfo(_CopySourceFolder & mySTObject.FilePaths.VaultPathFromRoot & mySTObject.FileName)

            '' FileInfo Destination
            If _DoCopyToVaultElseTempFolder Then
                myDestinationFile = New IO.FileInfo(mySTObject.FilePaths.VaultPath & mySTObject.FileName)
            Else
                myDestinationFile = New IO.FileInfo(_DestinationFolder & mySTObject.FilePaths.VaultPathFromRoot & mySTObject.FileName)
            End If

            WriteLog(STARTLOG)
            WriteLog("")
            WriteLog("Source file     : " & mySourceFile.FullName)
            WriteLog("Destination file: " & myDestinationFile.FullName)

            ' Check destination folder existance
            If Not _OnlyWriteBatchFile Then

                myDestinationDirectory = New IO.DirectoryInfo(myDestinationFile.DirectoryName)
                If Not myDestinationDirectory.Exists Then

                    WriteLog("Create Destination folder, does not yet exists:" & myDestinationDirectory.FullName)

                    ' if does not exist, create dir
                    If Not _DoOnlyLogNoActionsOnFiles Then
                        myDestinationDirectory.Create()
                        WriteLog("Created Destination folder: " & myDestinationFile.DirectoryName)
                    End If

                End If
            End If

            ' Check file existance
            DoGoOn = True
            If _OnlyWriteBatchFile Then
                ' We do not check if source and / or destination exists
            Else
                ' Check Destination
                If myDestinationFile.Exists Then
                    ' Skip it!
                    ReturnMsg = "Destination file exists so skipped"
                    Totals_DestExist += 1
                    DoGoOn = False
                End If
                ' Check Source
                If Not mySourceFile.Exists Then
                    ReturnMsg &= ",Source file not found so skipped"
                    Totals_SourceNotFound += 1
                    DoGoOn = False
                Else
                    Totals_SourceFound += 1
                End If
            End If
            If DoGoOn Then

                If _OnlyWriteBatchFile Or _DoOnlyLogNoActionsOnFiles Then

                    s = "xcopy " & XCopyParameters & " " & _
                         Chr(34) & mySourceFile.FullName & Chr(34) & " " & _
                         Chr(34) & myDestinationFile.FullName & Chr(34)

                    _BatchFileWriter.WriteLine(s)

                    ' Test Mode
                    FileIsCopied = True
                    ReturnMsg = "Test mode - File not copied"

                    TotalFilesCopied += 1

                Else
                    ' Copy file
                    WriteLog("..Copy file ..")
                    Try
                        Debug.WriteLine(myDestinationFile.Name)
                        mySourceFile.CopyTo(myDestinationFile.FullName)
                    Catch ex2 As Exception
                        ' Error!
                        WriteLog("Error copying file: " & ex2.Message)
                    End Try
                    If System.IO.File.Exists(myDestinationFile.FullName) Then
                        TotalFilesCopied += 1
                        ReturnMsg = "OK: Copied"
                        FileIsCopied = True
                    Else
                        TotalFilesFailed += 1
                        ReturnMsg = "Warning: File copy not succeeded"
                    End If

                End If
            End If

            ' File copy not succeeded
            WriteLog(ReturnMsg)

            ' Write the comma separated line
            WriteLog(WRITELOGLINE)

            Return FileIsCopied

        Catch ex As Exception
            ReturnMsg = "Error occured: " & ex.Message
            WriteLog(WRITELOGLINE)
            WriteLog(STARTLOG)
            WriteLog(ReturnMsg)
            WriteLog(WRITELOGLINE)
            Return False
        Finally
            MainForm.lblTotalDestAlreadyExists.Text = Totals_DestExist.ToString
            MainForm.lblTotalsFilesNotFound.Text = Totals_DestExist.ToString
            MainForm.lblTotalsFilesFound.Text = Totals_SourceFound.ToString
            MainForm.lblTotalFilesCopied.Text = TotalFilesCopied.ToString
        End Try
    End Function


    Private Sub WriteLog(ByVal s As String)

        Dim LogWriter As IO.StreamWriter = Nothing

        Try
            If _LogFile Is Nothing Then
                _LogFile = IO.Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "CopyFilesLog.txt")
            End If

            ' Init new logline
            If s = STARTLOG Then
                _LogLine = Now.ToString("dd/MM/yyyy HH:mm:ss") & ","
                Return
            ElseIf s = WRITELOGLINE Then
                ' Log now
                If Me._LogCommaSeparated Then
                    LogWriter = New IO.StreamWriter(_LogFile, True)
                    LogWriter.WriteLine(_LogLine)
                    LogWriter.Close()
                End If
                ' Init new line
                _LogLine = ""
                Return
            End If

            If _LogCommaSeparated Then
                ' Just add to _LogLine string
                If s.Length > 0 Then _LogLine &= s & ","
            Else
                ' Log now
                LogWriter = New IO.StreamWriter(_LogFile, True)
                LogWriter.Write(Now.ToString("dd/MM/yyyy HH:mm:ss"))
                LogWriter.Write(vbTab)
                LogWriter.WriteLine(s)
                LogWriter.Close()
            End If

            Return

        Catch ex As Exception
            '
            ' Event could not be written to the log...
            '
        Finally
            ' 
        End Try
    End Sub


End Class
