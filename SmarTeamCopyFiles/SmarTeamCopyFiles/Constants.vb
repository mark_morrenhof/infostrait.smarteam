Friend Module Constants

    Friend Const SQLSERVERDB As Boolean = True

    ' LifeCycle States
    Friend Const STATE_NEW As Integer = 0
    Friend Const STATE_CHECKED_IN As Integer = 1
    Friend Const STATE_CHECKED_OUT As Integer = 2
    Friend Const STATE_RELEASED As Integer = 3
    Friend Const STATE_OBSOLETE As Integer = 4

    Friend Const UNDETERMINED As String = "Undetermined"

    Friend Const CLASSIDDRAWINGS_FLAG As Short = 999

    ' The 7 Link classes
    Friend Const CLASS_SW_EDRAWINGOF As String = "eDrawing Of"
    Friend Const CLASS_SW_DERIVEDPART As String = "SolidWorks Derived Part"
    Friend Const CLASS_SW_DRAWINGOF As String = "SolidWorks Drawing of"
    Friend Const CLASS_SW_INCONTEXT As String = "SolidWorks In Context"
    Friend Const CLASS_SW_RAPIDDRAFT As String = "SolidWorks RapidDraft"
    Friend Const CLASS_SW_REFERENCE_TABLE As String = "TDM_SW_REFERENCE"  ' Reference"
    Friend Const CLASS_SW_SUPPRESSED As String = "SolidWorks Suppressed"


End Module
