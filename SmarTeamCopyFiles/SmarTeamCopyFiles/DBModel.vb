Imports SmarTeamCopyFiles.SharedObjects

Friend Class DBModel

    Friend VaultsDict As New Dictionary(Of Integer, FilePaths)

    ' SolidWorks Classes
    Friend SWAssemblyClasses As New Dictionary(Of Integer, ISmClass)
    Friend SWPartClasses As New Dictionary(Of Integer, ISmClass)
    Friend SWDrawingClasses As New Dictionary(Of Integer, ISmClass)
    Friend SWDrawing_OF_Classes As New Dictionary(Of Integer, ISmClass)

    Private _SolidWorksSuperClass_NotUsed, _SolidWorksSuperClassTree_NotUsed As String

    ' SmarTeam classes; (set using selected SmObject)
    Friend STClass_Super As ISmClass
    Friend STClass_Tree As ISmClass

    ' Attributes of class
    Friend AttribValue As String
    Friend AttribDescription As String
    Friend AttribOrder As String

    Friend VaultClassID As Short

    ' The 7 Link classes
    Friend STClass_SW_eDrawingOf As ISmClass
    Friend STClass_SW_DerivedPart As ISmClass
    Friend STClass_SW_DrawingOf As ISmClass
    Friend STClass_SW_InContext As ISmClass
    Friend STClass_SW_RapidDraft As ISmClass
    Friend STClass_SW_Reference As ISmClass
    Friend STClass_SW_Suppressed As ISmClass


    Friend Sub New()

        VaultClassID = STSession.MetaInfo.SmClassByName("Vaults Description").ClassId

        STClass_SW_eDrawingOf = STSession.MetaInfo.SmClassByName(CLASS_SW_EDRAWINGOF)
        STClass_SW_DerivedPart = STSession.MetaInfo.SmClassByName(CLASS_SW_DERIVEDPART)
        STClass_SW_DrawingOf = STSession.MetaInfo.SmClassByName(CLASS_SW_DRAWINGOF)
        STClass_SW_InContext = STSession.MetaInfo.SmClassByName(CLASS_SW_INCONTEXT)
        STClass_SW_RapidDraft = STSession.MetaInfo.SmClassByName(CLASS_SW_RAPIDDRAFT)
        STClass_SW_Reference = STSession.MetaInfo.SmClassByTableName(CLASS_SW_REFERENCE_TABLE)
        STClass_SW_Suppressed = STSession.MetaInfo.SmClassByName(CLASS_SW_SUPPRESSED)

        '' 170613: Also get all SolidWorks classes fromn TDM_CLASS
        GetSolidWorksClasses()

    End Sub


    Friend Function GetVaultPathByID(ByVal VaultID As Integer) As FilePaths

        ' Set Path properties in the FilePaths

        Try
            If Not VaultsDict.ContainsKey(VaultID) Then

                ' Add new to dict

                Dim STVaultObject As ISmObject
                Dim s As String = ""

                Dim myFPS As New FilePaths


                If VaultID > 0 Then
                    STVaultObject = STSession.ObjectStore.RetrieveObject(VaultClassID, VaultID)
                    If STVaultObject IsNot Nothing Then
                        myFPS.VaultPathFromRoot = STVaultObject.Data.ValueAsString("ROOT_DIR_ON_SRV")
                        If myFPS.VaultPathFromRoot Is Nothing Then
                            myFPS.VaultPathFromRoot = ""
                        Else
                            If Not myFPS.VaultPathFromRoot.EndsWith("\") Then myFPS.VaultPathFromRoot &= "\"

                            ' Get DestinationVaultRoot

                            STQuery1.SelectStatement = "SELECT SRV_SHARE_UNIT FROM TDM_VAULT_SERVERS WHERE OBJECT_ID = " & STVaultObject.Data.ValueAsInteger("SRV_OBJECT_ID")
                            STQuery1.Run()
                            If STQuery1.QueryResult.RecordCount > 0 Then
                                ' Concatenate
                                myFPS.VaultPath = STQuery1.QueryResult.ValueAsString("SRV_SHARE_UNIT", 0) & myFPS.VaultPathFromRoot

                                ' Correct Vault info
                                myFPS.IsVaultPath = True
                            Else
                                ' Should not happen, no feedback
                            End If
                        End If
                    End If
                End If
                VaultsDict.Add(VaultID, myFPS)
            End If

            ' Return value
            Return VaultsDict.Item(VaultID)

        Catch ex As Exception
            Throw New Exception("Error in GetVaultPathByID: " & ex.Message & "]")
        End Try
    End Function



    Private Sub GetSolidWorksClasses()


        ' Description: Get SolidWorks classes from data model
        ' Invoked by : Server.Initialise

        Dim s As String = ""
        Dim sql As String
        Dim ClassesFound As Integer
        Dim STQ As ISmSimpleQuery

        Try

            STQ = STSession.ObjectStore.NewSimpleQuery
            STQ.RunQueryMode = RunQueryModeEnum.rqmDirect


            '
            ' Retrieve table names of the Superclass, SolidWork Part, Assembly, Drawing, Drawing Of
            '
            sql = _
            "select cls.CLASS_ID, cls.CLASS_NAME, cls.TABLE_NAME, cls.CLASS_PREFIX, mech.MECHANISM_NAME" & _
            " from TDM_CLASS cls , TDM_MECHANISM_TYPE mech" & _
            " where "

            sql &= "cls.CLASS_ID in" & _
            "(select CLASS_ID from TDM_CLASS_MECHANISM where MECHANISM_TYPE in" & _
            "(select OBJECT_ID from TDM_MECHANISM_TYPE where MECHANISM_NAME in" & _
            "('FILE_CONTROL', 'TDM_SW_PART', 'TDM_SW_ASSEMBLY', 'TDM_SW_DRAWING', 'TDM_SW_DRAWINGOF')))" & _
            " and" & _
            " mech.OBJECT_ID in" & _
            "(select MECHANISM_TYPE from TDM_CLASS_MECHANISM where CLASS_ID=cls.CLASS_ID)" & _
            " and mech.MECHANISM_NAME in" & _
            "('FILE_CONTROL', 'TDM_SW_PART', 'TDM_SW_ASSEMBLY', 'TDM_SW_DRAWING', 'TDM_SW_DRAWINGOF')"

            STQ.SelectStatement = sql
            STQ.Run()

            For i As Integer = 0 To STQ.QueryResult.RecordCount - 1

                s = STQ.QueryResult.ValueAsString("MECHANISM_NAME", i)
                If s = "FILE_CONTROL" Then

                    '' Skip internal classes like TDM_CMT_LFC_FILE_PACKAGE
                    If STQ.QueryResult.ValueAsString("TABLE_NAME", i).StartsWith("TDM_") Then
                        ' skip
                    Else
                        ' use
                        _SolidWorksSuperClassTree_NotUsed = STQ.QueryResult.ValueAsString("CLASS_PREFIX", i) & "_TREE"
                        _SolidWorksSuperClass_NotUsed = STQ.QueryResult.ValueAsString("TABLE_NAME", i)
                        ClassesFound += 1
                    End If
                End If
            Next i

            '  Check results
            If ClassesFound = 0 Then
                Throw New NotImplementedException("No SuperClasses with File control " & s & " ")
            ElseIf ClassesFound > 1 Then
                Throw New NotImplementedException("Multiple SuperClasses with File control (with prefix other than TN_) is not supported by this version " & s & " ")
            End If


            ''' Get the SuperClass with File Control  (in case multiple classes use setting DocumentsSuperClassName 
            'selection = dt.Select("mechanism_name='FILE_CONTROL'")
            'If selection.Length = 0 Then
            '    Throw New ApplicationException("Cannot find SuperClass with File Control mechanism")
            'ElseIf selection.Length > 1 Then

            '    ' 160810: In case multiple classes, use the class as stored in config
            '    testclass = ConfigurationManager.AppSettings.Item("DocumentsSuperClassName")
            '    If testclass Is Nothing Then testclass = ""

            '    ' For STORK use first without 
            '    For Each row As DataRow In selection
            '        s &= row.Item("TABLE_NAME").ToString & vbLf


            '        '' Skip internal classes like TDM_CMT_LFC_FILE_PACKAGE
            '        If row.Item("TABLE_NAME").ToString.StartsWith("TDM_") Then
            '            ' skip
            '        Else
            '            ' 160810: use class
            '            If testclass.Length > 0 Then
            '                ' Check if the correct class
            '                If row.Item("CLASS_NAME").ToString = testclass Then
            '                    ' OK: use and exit loop
            '                    _SolidWorksSuperClassTree = row.Item("CLASS_PREFIX").ToString() & "_TREE"
            '                    _SolidWorksSuperClass = row.Item("TABLE_NAME").ToString
            '                    ClassesFound = 1
            '                    Exit For
            '                End If
            '            Else
            '                ' Use this class
            '                If _SolidWorksSuperClassTree Is Nothing Then
            '                    _SolidWorksSuperClassTree = row.Item("CLASS_PREFIX").ToString() & "_TREE"
            '                    _SolidWorksSuperClass = row.Item("TABLE_NAME").ToString
            '                End If
            '                ClassesFound += 1
            '            End If
            '        End If
            '    Next row

            '    'MsgBox(s, , "test message classes File Control")


            '    '  Check results
            '    If _SolidWorksSuperClassTree Is Nothing Then
            '        Throw New NotImplementedException("No SuperClasses with File control " & s & " ")
            '    ElseIf ClassesFound > 1 Then
            '        Throw New NotImplementedException("Multiple SuperClasses with File control (with prefix other than TN_) is not supported by this version " & s & " ")
            '    End If
            'Else
            '    ' OK. 1 class found
            '    _SolidWorksSuperClassTree = selection(0).Item("class_prefix").ToString() & "_TREE"
            '    _SolidWorksSuperClass = selection(0).Item("table_name").ToString
            'End If
            'selection = Nothing


            ' Classes SolidWorks Assembly
            Dim ClassId As Short
            Dim SmClass As ISmClass

            For i As Integer = 0 To STQ.QueryResult.RecordCount - 1

                ClassId = STQ.QueryResult.ValueAsSmallInt("CLASS_ID", i)
                SmClass = STSession.MetaInfo.SmClass(ClassId)

                s = STQ.QueryResult.ValueAsString("MECHANISM_NAME", i)
                Select Case s
                    Case "TDM_SW_ASSEMBLY"
                        SWAssemblyClasses.Add(ClassId, SmClass)
                    Case "TDM_SW_PART"
                        SWPartClasses.Add(ClassId, SmClass)
                    Case "TDM_SW_DRAWING"
                        SWDrawingClasses.Add(ClassId, SmClass)
                    Case "TDM_SW_DRAWINGOF"
                        SWDrawing_OF_Classes.Add(ClassId, SmClass)
                End Select
            Next i

            ' Finished
            Return

        Catch ex As Exception
            Throw New Exception("[Error in GetSolidWorksClasses: " & ex.Message & "]")
        End Try
    End Sub


End Class

Class FilePaths

    Friend IsVaultPath As Boolean = False

    ' File Paths without filenames
    Friend VaultPathFromRoot As String = ""  ' second part
    Friend VaultPath As String = ""          ' complete path

    ' FileName
    ' Friend FileName As String

End Class