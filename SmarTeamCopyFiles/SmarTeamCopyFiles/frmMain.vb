Public Class frmMain

    ' VERSION 031010: ..


    ' TODO: Settings from xml : Load / Save
    '       Loop tree, copy files           OK
    '       Loop Tree, check file exists    OK
    ' 
    '       Copy file only once (dest file exists handles this?)
    '       Late binding?
    '       Testen

    ' NICE TO HAVE:
    '       All revisions as separate son nodes?

    Private LGen As New General

    Private DoLoadTreeOnStartup As Boolean = False


    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            ' 301110: Added but not supported!
            ' chkCreateBatchFile.Enabled = False

            LGen.MainForm = Me

            Me.Visible = True
            SetState("Initialising ..")

            If Not LGen.GetSmarTeamSession Then
                SetState("No valid SmarTeam session ..")
                Return
            End If

            ' Get from Settings:
            txtSourceFolder.Text = My.Settings.SourceVaultRootFolder
            txtTempFolder.Text = My.Settings.DestinationTempFolder
            LGen.XCopyParameters = My.Settings.XCopyParameters
            LGen.DescriptionAttributeFromConfig = My.Settings.DescriptionAttribute


            chkShowFilePath.Checked = True
            ' chkShowFileExists.Checked = True



            If DoLoadTreeOnStartup Then
                SetState("Loading trees ..")
                If LGen.GetSelected > 0 Then
                    RefreshTree("Selected objects")
                End If
                SetState("-")
            Else
                SetState("Now select tree options and click " & Me.btnRefreshTree.Text & " ..")
            End If

        Catch ex As Exception
            MessageBox.Show("Error in btnRefreshTree_Click:" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub


    Private Sub btnRefreshTree_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshTree.Click
        RefreshTree("Objects")
    End Sub

    Private Sub RefreshTree(ByVal Operation As String)

        ' TODO: Only refresh display data, do not query

        Dim TreeClasses As String

        Try
            btnCopy.Enabled = False
            Tree1.Nodes.Clear()
            Me.Refresh()


            SetState("Get selected ..")
            If LGen.GetSelected > 0 Then

                '  30-11-2010
                TreeClasses = LGen.GetSWLinkClasses


                SetState("Loading info ..")
                LGen.LoadSmObjectsInTreeView("Selected objects", TreeClasses)
            End If

            Tree1.ExpandAll()
            SetState("-")
            Return

        Catch ex As Exception
            MessageBox.Show("Error in btnRefreshTree_Click:" & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            btnCopy.Enabled = True
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click

        Try
            ' TODO: Check if in Copy Action
            End

        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopy.Click

        ' Description: 

        Dim FilesCopied As Integer
        Dim DoCopyToVaultElseTempFolder As Boolean
        Dim DestFolder As String

        Try
            SetState("Start copying files ..")

            btnCopy.Enabled = False

            DestFolder = ""
            If radCopyToTestVault.Checked Then
                DoCopyToVaultElseTempFolder = True
            Else
                DoCopyToVaultElseTempFolder = False
                DestFolder = txtTempFolder.Text.Trim

                ' Check Destination folder exists, only in case not in batch file mode
                If Not chkCreateBatchFile.Checked Then
                    Dim f As New System.IO.DirectoryInfo(DestFolder)
                    If Not f.Exists Then
                        SetState("Copy aborted ..")
                        MessageBox.Show("First create folder " & DestFolder, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Return
                    End If
                End If
            
            End If

            SetState("Copying files, please wait ..")

            FilesCopied = LGen.CopyAllFilesFromVault(chkOnlyLogActions.Checked, _
                                                     chkCopyRevisions.Checked, _
                                                     txtSourceFolder.Text, txtTempFolder.Text, _
                                                     DoCopyToVaultElseTempFolder)

            'FilesCopied = LGen.CopyFilesToTestVault(myRecordlist, _
            '                            myRecordlist, txtSourceFolder.Text, DestFolder, _
            '                            DoCopyToVaultElseTempFolder, _
            '                            chkCopyRevisions.Checked)


            SetState("Ready copying files, files copied: " & FilesCopied & " (see the logfile)")
            Return

        Catch ex As Exception
            MessageBox.Show("Error in btnCopy_Click: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            btnCopy.Enabled = True
        End Try

    End Sub

    Friend Sub SetState(ByVal s As String)
        lblState.Text = s
        lblState.Refresh()
        System.Windows.Forms.Application.DoEvents()
    End Sub


    Private Sub btnCopyText_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopyText.Click

        Try
            If Me.Tree1.SelectedNode IsNot Nothing Then
                My.Computer.Clipboard.SetText(Tree1.SelectedNode.Text)
                SetState("Treenode text set to clipboard ..")
            End If

        Catch
        End Try
    End Sub

    Private Sub Label5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub GroupBox4_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox4.Enter

    End Sub

    Private Sub btnAllLinks_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAllLinks.Click

        chkLinkSolidWorksSuppressed.Checked = True
        chkLinkSolidWorksInContext.Checked = True
        chkLinkSolidWorksDerivedPart.Checked = True
        chkLinkedeDrawingOf.Checked = True
        chkLinkSolidWorksDrawingOf.Checked = True
        chkLinkSolidWorksRapidDraft.Checked = True
        chkLinkSolidWorksReference.Checked = True

    End Sub

    Private Sub chkLinkSolidWorksSuppressed_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkLinkSolidWorksSuppressed.CheckedChanged

    End Sub
End Class
